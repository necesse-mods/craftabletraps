Lets you craft traps in [**Necesse**](https://store.steampowered.com/app/1169040/Necesse/).

Available in [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=3129640759).